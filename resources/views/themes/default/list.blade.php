@extends('pickone::themes.default.layout')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">

            <div class="mt-3">
                <h5 class="m-0 py-4 border-bottom text-muted">文章列表</h5>
                @foreach ($posts as $post)
                    <div class="pt-4 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
                                @if ($post->thumbnail_url)
                                    <div class="text-center pb-4">
                                        <img src="{{ $post->thumbnail_url }}" style="max-width: 100%; max-height: 150px;">
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-8">
                                <div class="pb-4">
                                    <a href="/blog/view/{{ $post->id }}"><h5 class="m-0">{{ $post->title }}</h5></a>
                                    <div class="text-muted mt-3">{{ $post->created_at->format('Y-m-d') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="mt-4"></div>
</div>

@endsection
