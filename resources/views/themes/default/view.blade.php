@extends('pickone::themes.default.layout')

@section('head')

<title>{{ $post->title }}</title>

<meta name="description" content="{{ $post->meta_description }}">
<meta name="keywords" content="{{ $post->meta_keywords }}">

<meta property='og:title' content="{{ $post->title }}">
<meta property='og:description' content="{{ $post->meta_description }}">
<meta property='og:image' content="{{ $post->thumbnail_url }}">

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="mt-5">
                <h1 class="mb-4"><b>{{ $post->title }}</b></h1>
                <div class="text-muted mb-4">{{ $post->created_at->format('Y-m-d') }}</div>
                <div>
                    {!! $post->content !!}
                </div>

                @if ($post->tags->count())
                <div class="mt-4">
                    @foreach ($post->tags as $tag)
                        <span class="badge badge-secondary">{{ $tag->name }}</span>
                    @endforeach
                </div>
                @endif

                @if ($post->relatedPosts()->count())
                <hr class="my-4">
                <div class="mt-4">
                    相關文章
                </div>
                <div class="mt-3">
                    <div class="row">
                        @foreach ($post->relatedPosts as $p)
                        <div class="col-md-4">
                            <a href="/blog/view/{{ $p->id }}">
                                <div style="height: 150px;" class="mb-3">
                                    @if ($p->thumbnail_url)
                                        <div class="text-center">
                                            <img src="{{ $p->thumbnail_url }}" style="max-width: 100%; max-height: 150px;">
                                        </div>
                                    @endif
                                </div>
                            </a>
                            <div style="height: 6rem;">
                                <a href="/blog/view/{{ $p->id }}">{{ $p->title }}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif

                <hr class="my-4">

                <div id="disqus_thread"></div>
                <script>

                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

                var disqus_config = function () {
                this.page.url = '{{ url('blog/view/' . $post->id) }}';  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = '{{ $post->id }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };

                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://{{ config('blog.disqus-shortname') }}.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

            </div>
        </div>
    </div>
    <div class="my-5"></div>

</div>

@endsection
