<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--
    <link rel="stylesheet" type="text/css" href="/vendor/pickone/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="/vendor/pickone/css/open-color.css">

    <script src='/vendor/pickone/js/jquery-3.3.1.min.js'></script>
    <script src='/vendor/pickone/js/popper-1.14.0.min.js'></script>
    <script src='/vendor/pickone/js/bootstrap.min.js'></script>

    <link rel="stylesheet" type="text/css" href="/vendor/pickone/fontawesome-5.6.3/css/all.css">
    -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.2/autosize.min.js"></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:700%7CNunito:300,600" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="/vendor/pickone/themes/webmag/css/bootstrap.min.css"/>

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="/vendor/pickone/themes/webmag/css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="/vendor/pickone/themes/webmag/css/style.css"/>

    <script src="/vendor/pickone/themes/webmag/js/jquery.min.js"></script>

    @yield('head')

</head>
<body>

    @include('pickone::themes.webmag.header')

    @yield('content')

    @include('pickone::themes.webmag.footer')

    <!-- jQuery Plugins -->
    <script src="/vendor/pickone/themes/webmag/js/bootstrap.min.js"></script>
    <script src="/vendor/pickone/themes/webmag/js/main.js"></script>

</body>
</html>
