<!-- Header -->
<header id="header">
  <!-- Nav -->
  <div id="nav">
    <!-- Main Nav -->
    <div id="nav-fixed">
      <div class="container">
        <!-- logo -->
        <div class="nav-logo">
          <!--
          <a href="index.html" class="logo"><img src="./img/logo.png" alt=""></a>
          -->
          <a href="/blog" class="logo">Pickone Blog</a>
        </div>
        <!-- /logo -->

        <!-- nav -->
        <!--
        <ul class="nav-menu nav navbar-nav">
          <li><a href="category.html">News</a></li>
          <li><a href="category.html">Popular</a></li>
          <li class="cat-1"><a href="category.html">Web Design</a></li>
          <li class="cat-2"><a href="category.html">JavaScript</a></li>
          <li class="cat-3"><a href="category.html">Css</a></li>
          <li class="cat-4"><a href="category.html">Jquery</a></li>
        </ul>
        -->
        <!-- /nav -->

        <!-- search & aside toggle -->
        <!--
        <div class="nav-btns">
          <button class="aside-btn"><i class="fa fa-bars"></i></button>
          <button class="search-btn"><i class="fa fa-search"></i></button>
          <div class="search-form">
            <input class="search-input" type="text" name="search" placeholder="Enter Your Search ...">
            <button class="search-close"><i class="fa fa-times"></i></button>
          </div>
        </div>
        -->
        <!-- /search & aside toggle -->
      </div>
    </div>
    <!-- /Main Nav -->

    <!-- Aside Nav -->
    <div id="nav-aside">
      <!-- nav -->
      <div class="section-row">
        <ul class="nav-aside-menu">
          <li><a href="index.html">Home</a></li>
          <li><a href="about.html">About Us</a></li>
          <li><a href="#">Join Us</a></li>
          <li><a href="#">Advertisement</a></li>
          <li><a href="contact.html">Contacts</a></li>
        </ul>
      </div>
      <!-- /nav -->

      <!-- widget posts -->
      <!--
      <div class="section-row">
        <h3>Recent Posts</h3>
        <div class="post post-widget">
          <a class="post-img" href="blog-post.html"><img src="./img/widget-2.jpg" alt=""></a>
          <div class="post-body">
            <h3 class="post-title"><a href="blog-post.html">Pagedraw UI Builder Turns Your Website Design Mockup Into Code Automatically</a></h3>
          </div>
        </div>

        <div class="post post-widget">
          <a class="post-img" href="blog-post.html"><img src="./img/widget-3.jpg" alt=""></a>
          <div class="post-body">
            <h3 class="post-title"><a href="blog-post.html">Why Node.js Is The Coolest Kid On The Backend Development Block!</a></h3>
          </div>
        </div>

        <div class="post post-widget">
          <a class="post-img" href="blog-post.html"><img src="./img/widget-4.jpg" alt=""></a>
          <div class="post-body">
            <h3 class="post-title"><a href="blog-post.html">Tell-A-Tool: Guide To Web Design And Development Tools</a></h3>
          </div>
        </div>
      </div>
      -->
      <!-- /widget posts -->

      <!-- social links -->
      <div class="section-row">
        <h3>Follow us</h3>
        <ul class="nav-aside-social">
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
        </ul>
      </div>
      <!-- /social links -->

      <!-- aside nav close -->
      <button class="nav-aside-close"><i class="fa fa-times"></i></button>
      <!-- /aside nav close -->
    </div>
    <!-- Aside Nav -->
  </div>
  <!-- /Nav -->

  @if (isset($post) && !isset($posts))
  <!-- Page Header -->
  <div id="_post-header" class="_page-header" style="padding-top: 40px; padding-bottom: 40px; border-bottom: 2px solid #F4f4f9; background: #eee;">
    <?php
    // <div class="background-img" style="background-image: url('{{ $post->thumbnail_url }}');"></div>
    ?>
    <div class="container">
      <div class="row">
        <div class="col-md-10">
          <h1 style="color: #333;">{{ $post->title }}</h1>
          <div class="post-meta">
            <!--
            <a class="post-category cat-2" href="category.html">JavaScript</a>
            -->
            <span class="post-date" style="color: #999;">{{ $post->created_at->format('Y-m-d') }}</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Page Header -->
  @endif

</header>
<!-- /Header -->
