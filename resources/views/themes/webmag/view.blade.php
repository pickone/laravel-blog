@extends('pickone::themes.webmag.layout')

@section('head')

<title>{{ $post->title }}</title>

<meta name="description" content="{{ $post->meta_description }}">
<meta name="keywords" content="{{ $post->meta_keywords }}">

<meta property='og:title' content="{{ $post->title }}">
<meta property='og:description' content="{{ $post->meta_description }}">
<meta property='og:image' content="{{ $post->thumbnail_url }}">

@endsection

@section('content')

<!-- section -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- Post content -->
            <div class="col-md-8">
                <div class="section-row sticky-container" style="padding-left: 0;">
                    <div class="main-post">
                        {!! $post->content !!}
                    </div>
                    @include('pickone::themes.webmag.share')
                    <!--
                    <div class="post-shares sticky-shares">
                        <a href="#" class="share-facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="share-twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="share-google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="share-pinterest"><i class="fa fa-pinterest"></i></a>
                        <a href="#" class="share-linkedin"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-envelope"></i></a>
                    </div>
                    -->

                    <hr class="my-4">

                    <div id="disqus_thread"></div>
                    <script>

                    /**
                    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

                    var disqus_config = function () {
                    this.page.url = '{{ url('blog/view/' . $post->id) }}';  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = '{{ $post->id }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                    };

                    (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = 'https://{{ config('blog.disqus-shortname') }}.disqus.com/embed.js';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                    })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                </div>

            </div>
            <!-- /Post content -->

            <!-- aside -->
            <div class="col-md-4">

                @if ($post->relatedPosts()->count())
                <!-- post widget -->
                <div class="aside-widget">
                    <div class="section-title">
                        <h2>相關文章</h2>
                    </div>

                    @foreach ($post->relatedPosts as $p)
                    <div class="post post-widget">
                        <a class="post-img" href="/blog/view/{{ $p->id }}"><img src="{{ $p->thumbnail_url }}" alt=""></a>
                        <div class="post-body">
                            <h3 class="post-title"><a href="/blog/view/{{ $p->id }}">{{ $p->title }}</a></h3>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- /post widget -->
                @endif

                @if ($post->tags->count())
                <!-- tags -->
                <div class="aside-widget">
                    <div class="tags-widget">
                        <ul>
                            @foreach ($post->tags as $tag)
                                <li><a href="#!">{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /tags -->
                @endif

            </div>
            <!-- /aside -->
        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /section -->

@endsection
