@extends('pickone::themes.webmag.layout')

@section('content')

<!-- section -->
<div class="section">
    <!-- container -->
    <div class="container">

        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>近期文章</h2>
                </div>
            </div>

            @foreach ($posts as $post)
            <!-- post -->
            <div class="col-md-4">
                <div class="post">
                    <a class="post-img" href="/blog/view/{{ $post->id }}"><img src="{{ $post->thumbnail_url }}" alt="" style="height: 200px; width: auto; display: block; margin-left: auto; margin-right: auto;"></a>
                    <div class="post-body">
                        <div class="post-meta">
                            <!--
                            <a class="post-category cat-1" href="category.html">Web Design</a>
                            -->
                            <span class="post-date">{{ $post->created_at->format('Y-m-d') }}</span>
                        </div>
                        <h3 class="post-title"><a href="/blog/view/{{ $post->id }}">{{ $post->title }}</a></h3>
                    </div>
                </div>
            </div>
            <!-- /post -->

            @if ($loop->iteration % 3 === 0)
            <div class="clearfix visible-md visible-lg"></div>
            @endif

            @endforeach

            {{ $posts->links() }}

        </div>
        <!-- /row -->

    </div>
    <!-- /container -->
</div>
<!-- /section -->

@endsection
