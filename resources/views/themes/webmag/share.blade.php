<span class="social-share-box">
  <iframe src="https://www.facebook.com/plugins/share_button.php?href={{ url('blog/view/' . $post->id) }}&layout=button&size=small&mobile_iframe=true&appId=105638563639345&width=64&height=28"
    width="64" height="28" style="border:none;overflow:hidden; vertical-align: middle;"
    scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

  <div class="line-it-button" data-lang="zh_Hant" data-type="share-a" data-url="{{ url('blog/view/' . $post->id) }}" style="display: none;"></div>
</span>

<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>

<script>
    $(document).ready(function(){
        LineIt.loadButton();
    });
</script>

<style>
    .social-share-box {
        display: flex;
    }
</style>
