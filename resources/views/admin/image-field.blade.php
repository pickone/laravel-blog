<div class="image-field">
    @if (isset($image) && $image)
    <img src="{{ $image }}" class="img-thumbnail mb-2">
    @else
    <img src="" class="img-thumbnail mb-2" style="display: none;">
    @endif
    <input type="file" class="d-none">
    <div class="mb-2 loading" style="display: none;"><i class="fas fa-spinner fa-pulse fa-2x"></i></div>
    <button type="button" class="btn btn-sencodary">上傳圖片</button>
    @if (isset($image) && $image)
    <input type="hidden" name="image" value="{{ $image }}">
    @else
    <input type="hidden" name="image" value="">
    @endif
</div>

<script>
    $(document).ready(function(){
        $('.image-field input[type="file"]').change(function(e){
            $('.image-field img').hide();
            $('.image-field .loading').show();
            uploadImage(this);
        });

        $('.image-field button').click(function(e){
            $(this).parent().find('input[type="file"]').click();
        })
    });

    function uploadImage(element)
    {
        var data = new FormData();

        data.append('image',element.files[0]);
        data.append('_token', $("meta[name='csrf-token']").attr("content"));
        $.ajax({
            type: "POST",
            url: "/admin/blog/image/upload",
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            data: data,
        }).done(function(data){
            $('.image-field img').attr('src', data.url);
            $('.image-field img').show();
            $('.image-field input[type="hidden"]').val(data.url);
        }).fail(function(res){
            if ($('.image-field img').attr('src')) {
                $('.image-field img').show();
            }

            if (res && res.responseJSON && res.responseJSON.message) {
                alert(res.responseJSON.message)
            } else {
                alert('抱歉，上傳失敗，煩請稍後再試一次。')
            }
        }).always(function(){
            $('.image-field .loading').hide();
        });
    }
</script>
