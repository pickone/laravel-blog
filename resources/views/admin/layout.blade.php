<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="/vendor/pickone/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="/vendor/pickone/css/open-color.css">

    <script src='/vendor/pickone/js/jquery-3.3.1.min.js'></script>
    <script src='/vendor/pickone/js/popper-1.14.0.min.js'></script>
    <script src='/vendor/pickone/js/bootstrap.min.js'></script>

    <link rel="stylesheet" type="text/css" href="/vendor/pickone/fontawesome-5.6.3/css/all.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/autosize.js/4.0.2/autosize.min.js"></script>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('head')

</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="{{ url(config('blog.admin-route')) }}">
            <b>Pickone Blog Admin</b>
        </a>
    </nav>

    @yield('content')

</body>
</html>
