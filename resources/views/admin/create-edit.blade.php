@extends('pickone::admin.layout')

@section('head')

<script src="/vendor/pickone/tinymce/tinymce.min.js"></script>
<script src="/vendor/pickone/tinymce/langs/zh_TW.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="/vendor/pickone/tag-it/tag-it.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="/vendor/pickone/tag-it/jquery.tagit.css" rel="stylesheet" type="text/css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="mt-4">
                @if (isset($post))
                <form method="post" action="/admin/blog/submit-edit">
                @else
                <form method="post" action="/admin/blog/submit-create">
                @endif
                    @if (isset($post))
                    <h4 class="mb-3">編輯文章</h4>
                    @else
                    <h4 class="mb-3">新增文章</h4>
                    @endif

                    @if (isset($post))
                    <input type="text" class="form-control mb-3" placeholder="title" name="title" value="{{ $post->title }}" required>
                    @else
                    <input type="text" class="form-control mb-3" placeholder="title" name="title" required>
                    @endif

                    @if (isset($post))
                    <textarea class="form-control" placeholder="content" name="content">{{ $post->content }}</textarea>
                    @else
                    <textarea class="form-control" placeholder="content" name="content"></textarea>
                    @endif

                    <div class="mt-3">
                        <div class="mb-2">文章縮圖</div>
                        @if (isset($post))
                        @include('pickone::admin.image-field', ['image' => $post->thumbnail_url])
                        @else
                        @include('pickone::admin.image-field')
                        @endif
                    </div>

                    <div class="mt-3">
                        <div class="mb-2">標籤</div>
                        <ul id="myTags">
                            @if (isset($post))
                                @foreach ($post->tags as $tag)
                                    <li>{{ $tag->name }}</li>
                                @endforeach
                            @else

                            @endif
                        </ul>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $("#myTags").tagit({
                                    fieldName: "tags[]",
                                    availableTags: [
                                        @foreach (Pickone\Blog\Tag::all() as $tag)
                                            {!! json_encode($tag->name) !!},
                                        @endforeach
                                    ]
                                });
                            });
                        </script>
                    </div>

                    <div class="mt-3">
                        <div class="mb-2">SEO Meta Tag</div>

                        @if (isset($post))
                        <input type="text" class="form-control mt-2" placeholder="meta descsription" name="meta_description" value="{{ $post->meta_description }}">
                        @else
                        <input type="text" class="form-control mt-2" placeholder="meta descsription" name="meta_description">
                        @endif

                        @if (isset($post))
                        <input type="text" class="form-control mt-2" placeholder="meta keywords" name="meta_keywords" value="{{ $post->meta_keywords }}">
                        @else
                        <input type="text" class="form-control mt-2" placeholder="meta keywords" name="meta_keywords">
                        @endif

                    </div>

                    <div class="mt-3">
                        <div>文章狀態</div>
                        <select class="form-control mt-2" name="status">
                            <option value="1" @if (isset($post) && $post->status == 1) selected @endif>存為草稿</option>
                            <option value="2" @if (isset($post) && $post->status == 2) selected @endif>立即發佈</option>
                            <option value="3" @if (isset($post) && $post->status == 3) selected @endif>下架隱藏</option>
                        </select>
                    </div>

                    <div class="mt-3 schedule-box">
                        <div class="mb-2">排程發佈時間</div>
                        @if (isset($post) && $post->getPublishedAt())
                        <input type="text" class="form-control mt-2" name="datetime" value="{{ $post->getPublishedAt()->format('Y-m-d H:i') }}">
                        @else
                        <input type="text" class="form-control mt-2" name="datetime">
                        @endif

                        <script>
                            $(document).ready(function(){
                                $('input[name="datetime"]').datetimepicker({
                                  format:'Y-m-d H:i',
                                  inline:true,
                                });
                            });
                        </script>

                    </div>

                    @if (isset($post))
                    <input type="hidden" name="id" value="{{ $post->id }}">
                    @endif

                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-primary btn-block mt-3" value="送出">
                </form>
            </div>
        </div>
    </div>

    <div class="mt-4"></div>
</div>

<script>
    $(document).ready(function(){
        tinymce.init({
            selector: 'textarea[name="content"]',
            plugins: 'image link media table hr advlist lists textcolor imagetools autoresize',
            toolbar: 'undo redo | styleselect | fontsizeselect bold italic strikethrough forecolor backcolor removeformat | link | numlist bullist | image media',
            language: 'zh_TW',
            branding: false,
            autoresize_min_height: 400,
            autoresize_bottom_margin: 5,
            file_picker_types: 'image',
            // for image
            image_description: false,
            image_dimensions: false,
            relative_urls: false,
            file_browser_callback: function(field_name, url, type, win) {
                // trigger file upload form
                if (type == 'image') $('#formUpload input').click();
            },
            // for media
            media_alt_source: false,
            media_poster: false,
            media_dimensions: false,
        });

        $('select[name="status"]').change(function(){
            if ($(this).val() != 1) {
                $('.schedule-box').hide();
            } else {
                $('.schedule-box').show();
            }
        });

        @if (isset($post))
            $('select[name="status"]').trigger('change');
        @endif
    });
</script>

<iframe id="frameUpload" name="frameUpload" style="display:none"></iframe>
<form id="formUpload" action="/admin/blog/tinymce/upload-image" target="frameUpload" method="post" enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
    <input name="image" type="file" onchange="$('#formUpload').submit();this.value='';">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
</form>

<style>
    .image-field img {
        display: block;
        max-width: 200px;
        max-height: 200px;
    }
</style>

@endsection
