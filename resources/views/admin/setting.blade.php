@extends('pickone::admin.layout')

@section('head')

@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="mt-4">
                <h3>相關文章設定：{{ $post->title }}</h3>
                <hr>
                <div>目前相關文章：</div>
                <ul class="mt-2">
                    @foreach ($post->relatedPosts as $p)
                    <li>
                        <a href="/admin/blog/edit/{{ $p->id }}">[{{ $p->id }}] {{ $p->title }}</a>
                        <form class="d-inline" method="post" action="/admin/blog/delete-relate">
                            {{ csrf_field() }}
                            <input type='hidden' name="post_id" value="{{ $post->id }}">
                            <input type='hidden' name="related_post_id" value="{{ $p->id }}">
                            <button class="btn btn-link">刪除</button>
                        </form>
                    </li>
                    @endforeach
                </ul>
                <hr>
                <div class="mt-3">
                    <form method="post" action="/admin/blog/submit-setting">
                        <div>新增相關文章：</div>
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        {{ csrf_field() }}
                        <select class="form-control mt-2" name="related_post_id">
                            @foreach ($posts as $p)
                                <option value="{{ $p->id }}">[{{ $p->id }}] {{ $p->title }}</option>
                            @endforeach
                        </select>
                        <input type="submit" class="btn btn-primary btn-block mt-3" value="新增">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="mt-4"></div>
</div>

@endsection
