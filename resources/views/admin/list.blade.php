@extends('pickone::admin.layout')

@section('content')

<div class="container">
    <div class="row">
        <div class="col">
            <div class="mt-4"><a href="/admin/blog/create" class="btn btn-primary">新增文章</a></div>

            <div class="table-responsive mt-3">
                <table class="table table-bordered">
                    <tr>
                        <th>ID</th>
                        <th>縮圖</th>
                        <th>標題</th>
                        <th>相關</th>
                        <th>狀態與日期</th>
                    </tr>
                    @foreach ($posts as $post)
                        <tr>
                            <td>
                                {{ $post->id }}
                            </td>
                            <td>
                            @if ($post->thumbnail_url)
                            <img src="{{ $post->thumbnail_url }}" style="max-height: 50px; max-width: 50px;">
                            @endif
                            </td>
                            <td>
                                <a href="/admin/blog/edit/{{ $post->id }}"><b>{{ $post->title }}</b></a>
                                @if ($post->status == Pickone\Blog\Post::STATUS_DRAFT)
                                <a href="/blog/view/{{ $post->id }}?preview=true" target="_blank" class="btn btn-outline-secondary btn-sm ml-1">預覽</a>
                                @endif
                                @if ($post->status == Pickone\Blog\Post::STATUS_PUBLISHED)
                                <a href="/blog/view/{{ $post->id }}" target="_blank" class="btn btn-outline-secondary btn-sm ml-1">查看</a>
                                @endif
                            </td>
                            <td>
                                <a href="/admin/blog/setting/{{ $post->id }}">設定</a>
                            </td>
                            <td>
                                <div class="">
                                    {{ $post->showStatus() }}
                                </div>
                                <!--
                                <div class="mt-1 text-muted">
                                    {{ $post->created_at->format('Y-m-d') }}
                                </div>
                                -->
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    <div class="mt-4"></div>
</div>

@endsection
