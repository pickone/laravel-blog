<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickoneBlogBasicTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pickone_blog_posts', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('user_id');
            $t->string('title');
            $t->text('content');
            $t->text('thumbnail_url');
            $t->string('meta_description');
            $t->string('meta_keywords');
            $t->datetime('published_at')->nullable();
            $t->integer('status');
            $t->timestamps();
        });

        Schema::create('pickone_blog_related_post', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('post_id');
            $t->integer('related_post_id');
            $t->timestamps();
        });

        Schema::create('pickone_blog_tags', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name');
            $t->timestamps();
        });

        Schema::create('pickone_blog_post_tag', function (Blueprint $t) {
            $t->integer('post_id');
            $t->integer('tag_id');
            $t->timestamps();

            $t->primary(['post_id', 'tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pickone_blog_posts');
        Schema::dropIfExists('pickone_blog_related_post');
        Schema::dropIfExists('pickone_blog_tags');
        Schema::dropIfExists('pickone_blog_post_tag');
    }
}
