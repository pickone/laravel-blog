# 如何開發

1. 先建立一個 laravel 專案（此專案不需要 git init，僅用來作為開發套件的環境）

2. 此專案安裝 https://github.com/Jeroen-G/laravel-packager

3. 輸入指令

```
artisan packager:new pickone blog
```

4. 將此 repo 內容 git clone 到 /packages/pickone/blog 內部。即可在裡面動手開發。
