# Overall schema

- user_id
- title
- content
- thumbnail_url
- meta_description
- meta_keywords
- published_at
- status

# 部落格模組

- 文章功能
- 標籤功能

- blog
- blog/view/1

# Treerful

- from blogger.com

- blog/list
- blog/article/3159743624083665318

## how to export blogger.com content?
  -
## how to redirect to new routes?
  - jump to homepage

# Pickone

## blog

- no
- space_no
- title
- cover
- content
- editor
- timestamps

## space_no ... how to add more fields in blog module?
  - other table is fine

### pickone_blog_post_space

- id
- pickone_blog_post_id
- space_no / space_id
- timestamps

- Space::findByBlogPostId($blogPost->id) -> return $space

### migrate commands

- blog:import-from-pickone
- from pickone@blog -> pickone@pickone_blog_posts & pickone@pickone_blog_post_space
- same database? different database?
  - old & new app use the same DB right?

## editor ... not user_id?
  - user_id
## migrate:
  - cover -> thumbnail_url

# Others

## write migrate commands for both treerful & pickone?
(v) 管理者登入帳號問題

(v) 部落格權限 -> pickone -> migrate into new schema
