# pickone_blog_posts

- id: INT [primary key]
- user_id
- title: string
- content: text
- thumbnail_url: text
- meta_description: string
- meta_keywords: string
- published_at: datetime, nullable
- status: int
- timestamps

> content 存的是 tinymce 生成的 html
> status: 1: 草稿 2: 已發佈 3: 已隱藏

# pickone_blog_related_post

- post_id: int
- related_post_id: int
- timestamps

> post_id 是當前文章，related_post_id 是出現在當前文章底部的推薦相關文章

# pickone_blog_tags

- id [primary key]
- name
- timestamps

# pickone_blog_post_tag

- post_id
- tag_id
- timestamps

>post_id + tag_id: UNIQUE KEY
