# Todo

- upload image error

- survey: 編輯界面/前端顯示，一模一樣
- refactor the namespaces?
  - config('blog.sample-key')
  - view('pickone::welcome')

# scripts

```
php artisan db:seed --class="Pickone\Blog\Seeds\DatabaseSeeder"
```

# Others

- notice one issue with the tag manager library: https://github.com/aehlke/tag-it/issues/370
- notice the release tag. start from v1.0.0
