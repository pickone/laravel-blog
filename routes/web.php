<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => config('blog.route'), 'namespace' => 'Pickone\Blog\Http\Controllers', 'middleware' => 'web'], function() {
    Route::get('/', 'BlogController@showList');

    Route::get('/view/{id}', 'BlogController@view');
});

Route::group(['prefix' => config('blog.admin-route'), 'namespace' => 'Pickone\Blog\Http\Controllers\Admin', 'middleware' => ['web', 'auth', 'blog.admin']], function() {
    Route::get('/', 'BlogController@showList');

    Route::get('/edit/{id}', 'BlogController@edit');

    Route::get('/create', 'BlogController@create');

    Route::post('/submit-create', 'BlogController@submitCreate');

    Route::post('/submit-edit', 'BlogController@submitEdit');

    Route::post('/tinymce/upload-image', 'BlogController@uploadImage');

    Route::post('/image/upload', 'BlogController@imageUploadSimple');

    Route::get('/setting/{id}', 'BlogController@setting');

    Route::post('/submit-setting', 'BlogController@submitSetting');

    Route::post('/delete-relate', 'BlogController@deleteRelate');
});

Route::get('/pickone/blog/connect', function () {
    echo '<p>This is from the package: pickone/blog</p>';

    echo '<p>Connect to the config file: ' . config('blog.sample-key') . '</p>';

    echo '<p>Connect to the PHP classes: ' . get_class(new Pickone\Blog\Seeds\DatabaseSeeder) . '</p>';
});

Route::get('/pickone/blog/welcome', function () {
    return view('pickone::welcome');
});
