<?php

return [
    'sample-key' => 'sample-value',
    // 'route' => 'pickone/blog',
    'route' => 'blog',
    'admin-route' => 'admin/blog',
    'disqus-shortname' => env('DISQUS_SHORTNAME'),
    'theme' => env('BLOG_THEME', 'webmag'),
    'admin-emails' => explode(',', env('BLOG_ADMIN_EMAILS', 'howtomakeaturn@hotmail.com')),
    'per-page' => 12,
];
