<?php

namespace Pickone\Blog\Http\Controllers\Admin;

use GuzzleHttp\Client;
use Pickone\Blog\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pickone\Blog\Blog;
use Pickone\Blog\Post;
use Pickone\Blog\Repositories\TagRepository;

class BlogController extends Controller
{
    protected $blog;
    protected $tagRepository;

    public function __construct(Blog $blog, TagRepository $tagRepository)
    {
        $this->blog = $blog;
        $this->tagRepository = $tagRepository;
    }

    function showList(Request $request)
    {
        $posts = $this->blog->getPosts();

        return view('pickone::admin.list', compact('posts'));
    }

    function edit(Request $request, $id)
    {
        $post = Post::find($id);

        return view('pickone::admin.create-edit', compact('post'));
    }

    function create(Request $request)
    {
        return view('pickone::admin.create-edit');
    }

    function submitCreate(Request $request)
    {
        $post = new Post();

        $this->handleRequest($post, $request);

        return redirect()->to('/admin/blog');
    }

    function submitEdit(Request $request)
    {
        $post = Post::find($request->get('id'));

        $this->handleRequest($post, $request);

        return redirect()->to('/admin/blog');
    }

    function handleRequest($post, $request)
    {
        $post->title = $request->get('title');

        $post->content = $request->get('content') ? : '';

        $post->meta_description = $request->get('meta_description') ? : '';

        $post->meta_keywords = $request->get('meta_keywords') ? : '';

        $post->thumbnail_url = $request->get('image') ? : '';

        $post->user_id = 0;

        $post->status = $request->get('status');

        $tags = $request->get('tags') ? : [];

        if ($request->get('status') == 1) {
            $post->published_at = $request->get('datetime');
        } else {
            $post->published_at = null;
        }

        $post->save();

        $this->tagRepository->updateTags($post, $tags);
    }

    public function uploadImage(Request $request)
    {
        $result = array();
        $image = $request->file('image');

        if (!$image) {
            $result['status'] = 'ERR';
            return response()->json($result);
        }

        $url = 'https://api.imgur.com/3/image.json';
        $data = file_get_contents($image);

        $parameter = [
            'headers' => ['Authorization' => 'Client-ID c09c97e33dec910'],
            'form_params' => ['image' => base64_encode($data)],
            'timeout' => 30,
            'verify' => false,
        ];

        $client = new Client();
        $response = json_decode($client->post($url, $parameter)->getBody(), true);
        $result['status'] = 'OK';
        $result['url'] = $response['data']['link'];

        return "
            <script>
                top.$('.mce-btn.mce-open').parent().find('.mce-textbox').val('" . $result['url'] . "').closest('.mce-window').find('.mce-primary').click();
            </script>
        ";
    }

    public function imageUploadSimple(Request $request)
    {
        $result = array();
        $image = $request->file('image');

        if (!$image) {
            $result['status'] = 'ERR';
            return response()->json($result);
        }

        $data = getimagesize($image);
        $width = $data[0];
        $height = $data[1];

        if ($width > 300 || $height > 300) {
            $manager = new \Intervention\Image\ImageManager();

            $manager->make($image)->resize(300, 300, function($constraint) {
                $constraint->aspectRatio();
            })->save();

            /*
            return response([
                'status' => 'error',
                'message' => '上傳失敗。請上傳尺寸小於 300x300 px 的圖片。',
            ], 400);
            */
        }

        $url = 'https://api.imgur.com/3/image.json';

        $data = file_get_contents($image);

        $parameter = [
            'headers' => ['Authorization' => 'Client-ID c77b01f7d49eebe'],
            'form_params' => ['image' => base64_encode($data)],
            'timeout' => 30,
            'verify' => false,
        ];

        $client = new Client();
        $response = json_decode($client->post($url, $parameter)->getBody(), true);
        $result['status'] = 'OK';
        $result['url'] = $response['data']['link'];

        return $result;
    }

    function setting(Request $request, $id)
    {
        $post = Post::find($id);

        $posts = $this->blog->getPosts();

        return view('pickone::admin.setting', compact('post', 'posts'));
    }

    function submitSetting(Request $request)
    {
        \DB::table('pickone_blog_related_post')->insert([
            'post_id' => $request->get('post_id'),
            'related_post_id' => $request->get('related_post_id'),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        return redirect()->back();
    }

    function deleteRelate(Request $request)
    {
        \DB::table('pickone_blog_related_post')->where([
            'post_id' => $request->get('post_id'),
            'related_post_id' => $request->get('related_post_id'),
        ])->delete();

        return redirect()->back();
    }
}
