<?php

namespace Pickone\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Pickone\Blog\Blog;
use Pickone\Blog\Post;

class BlogController extends Controller
{
    protected $blog;

    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }

    function showList(Request $request)
    {
        $posts = $this->blog->getPublishedPosts();

        $posts = $this->paginateCollection($posts, config('blog.per-page'));

        return view('pickone::themes.' . config('blog.theme') . '.list', compact('posts'));
    }

    function view(Request $request, $id)
    {
        $post = Post::find($id);

        if ($post->isPublic()) {
            return view('pickone::themes.' . config('blog.theme') . '.view', compact('post'));
        }

        if ($post->status == Post::STATUS_DRAFT && $request->get('preview')) {
            return view('pickone::themes.' . config('blog.theme') . '.view', compact('post'));
        }
    }
}
