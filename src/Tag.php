<?php

namespace Pickone\Blog;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'pickone_blog_tags';

    function postTags()
    {
        return $this->hasMany('Pickone\Blog\PostTag');
    }
}
