<?php

namespace Pickone\Blog;

use Illuminate\Database\Eloquent\Model;

class PostTag extends Model
{
    public $incrementing = false;

    protected $table = 'pickone_blog_post_tag';

    function tag()
    {
        return $this->belongsTo('Pickone\Blog\Tag');
    }

    function post()
    {
        return $this->belongsTo('Pickone\Blog\Post');
    }
}
