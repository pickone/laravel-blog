<?php

namespace Pickone\Blog\Seeds;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 30; $i++) {
            $this->seedPosts();
        }
    }

    function seedPosts()
    {
        $faker = \Faker\Factory::create();

        DB::table('pickone_blog_posts')->insert([
            'title' => $faker->name,
            'content' => $faker->paragraph,
            'user_id' => 0,
            'thumbnail_url' => 'https://source.unsplash.com/random',
            'meta_description' => '',
            'meta_keywords' => '',
            'published_at' => \Carbon\Carbon::now(),
            'status' => 2,
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
