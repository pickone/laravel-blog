<?php

namespace Pickone\Blog\Repositories;

use Pickone\Blog\Tag;
use Pickone\Blog\PostTag;
use DB;

class TagRepository extends BaseRepository
{
    function __construct(Tag $tag)
    {
        parent::__construct($tag);
    }

    function updateTags($post, $names)
    {
        foreach ($names as $name) {
            $tag = $this->findOrAdd($name);

            $this->insertPostTagIfNotExist($post->id, $tag->id);
        }

        foreach ($post->tags as $tag) {
            if (!in_array($tag->name, $names)) {
                PostTag::where('post_id', $post->id)->where('tag_id', $tag->id)->delete();
            }
        }
    }

    function findOrAdd($name)
    {
        $tag = Tag::where('name', $name)
            ->first();

        if ($tag) return $tag;

        $tag = new Tag();

        $tag->name = $name;

        $tag->save();

        return $tag;
    }

    function insertPostTagIfNotExist($postId, $tagId)
    {
        if ($postTag = PostTag::where('post_id', $postId)->where('tag_id', $tagId)->first()) {
            return $postTag;
        }

        $postTag = new PostTag();

        $postTag->post_id = $postId;

        $postTag->tag_id = $tagId;

        $postTag->save();

        return $postTag;
    }
}
