<?php

namespace Pickone\Blog;

class Blog
{
    function getPosts()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        return $posts;
    }

    function getPublishedPosts()
    {
        return $this->getPosts()->filter(function($post) {
            return $post->isPublic();
        });
    }
}
