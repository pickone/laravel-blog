<?php

namespace Pickone\Blog;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const STATUS_DRAFT = 1;

    const STATUS_PUBLISHED = 2;

    const STATUS_HIDDEN = 3;

    protected $table = 'pickone_blog_posts';

    function tags()
    {
        return $this->belongsToMany('Pickone\Blog\Tag', 'pickone_blog_post_tag');
    }

    function getPublishedAt()
    {
        if ($this->published_at == null) return null;

        return \Carbon\Carbon::parse($this->published_at);
    }

    function showStatus()
    {
        if ($this->status == self::STATUS_DRAFT) {
            if ($this->published_at == null) return '草稿';

            return '排程發佈：' . $this->getPublishedAt()->format('m-d H:i');
        }

        if ($this->status == self::STATUS_PUBLISHED) return '已發佈';

        if ($this->status == self::STATUS_HIDDEN) return '隱藏';
    }

    function relatedPosts()
    {
        return $this->belongsToMany('Pickone\Blog\Post', 'pickone_blog_related_post', 'post_id', 'related_post_id');

    }

    function isPublic()
    {
        if ($this->status == self::STATUS_PUBLISHED) return true;

        if ($this->status == self::STATUS_DRAFT && $this->getPublishedAt() !== null &&
            $this->getPublishedAt() < \Carbon\Carbon::now()) return true;

        return false;
    }
}
